const axios = require('axios')

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  vue: {
    config: {
      productionTip: true,
      devtools: false,
    },
  },

  loading: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'server',

  generate: {
    // ini generate static route untuk artikel
    routes() {
      return axios
        .get(
          `https://api.loalivingsolo.com/api/v1/public/articles?page=1&per_page=100&type=ARTICLE`
        )
        .then((res) => {
          return res.data.data.data.map((article) => {
            return {
              route: `/loa/${article.id}/${article.slug}`,
              payload: article,
            }
          })
        })
    },
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Loaliving',
    titleTemplate: '%s - Loaliving',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        name: 'google-site-verification',
        content: 'xbxEgG0xIvx2o1Dvg8mawSGOxi-RT7TzFDP1AVLOreg',
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Home & Lifestyle Passion for interior design',
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content:
          'hotel di Solo, LOA Living Solo Baru, hotel di Solo Baru, hotel dekat keraton Solo, hotel bintang 3 di Solo Baru, hotel bintang 3 di Solo, hotel bagus di Solo, hotel bagus di Solo Baru, hotel bersih di Solo, kamar hotel di Solo, kamar hotel di Solo Baru, akomodasi di Solo, penginapan di Solo, hotel dekat Hartono Mall, hotel di Hartono Mall, hotel di The Park, hotel dekat The Park',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'quill/dist/quill.core.css',
    // for snow theme
    'quill/dist/quill.snow.css',
    // for bubble theme
    'quill/dist/quill.bubble.css',
    '@/assets/main.css',
  ],

  router: {
    // middleware: ['theme'],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/viewer', ssr: false },
    { src: '~/plugins/quill', ssr: false },
    { src: '~/plugins/filter', ssr: false },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/sitemap',
    [
      'nuxt-compress',
      {
        gzip: {
          cache: true,
        },
        brotli: {
          threshold: 10240,
        },
      },
    ],
    '@nuxtjs/dotenv',
    ['@nuxtjs/robots'],
    [
      'nuxt-imagemin',
      {
        optipng: { optimizationLevel: 5 },
        gifsicle: { optimizationLevel: 2 },
      },
    ],
    ['nuxt-canonical', { baseUrl: 'https://loalivingsolo.com' }],
  ],

  // tailwindcss: {
  //   jit: true,
  // },

  robots: {
    UserAgent: '*',
    Allow: '/',
    Sitemap: 'https://www.loalivingsolo.com/sitemap.xml',
  },

  sitemap: {
    hostname: 'https://loalivingsolo.com',
    gzip: true,
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    // analyze: true,
    extractCSS: true,
    optimization: {
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.(css|vue|scss)$/,
            chunks: 'all',
            enforce: true,
          },
        },
      },
    },
    extend(config, { isClient }) {
      if (isClient) {
        config.optimization.splitChunks.maxSize = 200000
      }
    },
  },
}
