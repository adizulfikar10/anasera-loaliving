import { errorNotif, toFormData } from '../common/utils'
import { request, requestOpen } from '../plugins/axios'

import { generateQueryString } from '../common/queryGenerator'

export const state = () => ({
  section1: [],
  section2: [],
  section3: [],
})

export const getters = {
  getSection1(state) {
    return state.section1
  },
  getSection2(state) {
    return state.section2
  },
  getSection3(state) {
    return state.section3
  },
}

export const actions = {
  async fetchAbout(context, payload) {
    try {
      const query = generateQueryString(payload.params)
      const res = await requestOpen({
        url: `articles?${query}`,
        method: 'get',
      })
      const section = res.data.data.data
      context.commit(
        'setSection1',
        section.find((el) => el.section == 1)
      )
      context.commit(
        'setSection2',
        section.find((el) => el.section == 2)
      )
      context.commit(
        'setSection3',
        section.find((el) => el.section == 3)
      )
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },
  async update(context, payload) {
    try {
      const formData = toFormData(payload)
      await request({
        url: `articles/${payload.id}`,
        method: 'post',
        headers: {
          'Content-Type': 'multipart/formdata',
        },
        data: formData,
      })

      return {
        title: 'success',
        message: 'update data',
      }
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },
}

export const mutations = {
  setSection1(state, payload) {
    state.section1 = payload
  },

  setSection2(state, payload) {
    state.section2 = payload
  },

  setSection3(state, payload) {
    state.section3 = payload
  },
}
