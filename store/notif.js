export const state = () => ({
  title: '',
  message: '',
})
export const getters = {
  getNotif(state) {
    return state
  },
}
export const actions = {
  setNotif(context, payload) {
    context.commit('setNotif', payload)
  },
}

export const mutations = {
  setNotif(state, payload) {
    state.title = payload.title
    state.message = payload.message
  },
}
