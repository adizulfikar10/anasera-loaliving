import { request, requestOpen } from '../plugins/axios'

import { generateQueryString } from '../common/queryGenerator'
import { errorNotif, toFormData } from '../common/utils'

export const state = () => ({
  rooms: [],
})

export const getters = {
  getRooms(state) {
    return state.rooms
  },
}

export const actions = {
  async fetchRooms(context, payload) {
    try {
      const query = generateQueryString(payload.params)
      const res = await requestOpen({
        url: `rooms?${query}`,
        method: 'get',
      })
      context.commit('setRooms', res.data.data?.data)
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },

  async updateRooms(context, payload) {
    try {
      const data = toFormData(payload)
      const res = await request({
        url: `rooms/${payload.id}`,
        method: 'post',
        data: data,
        headers: {
          'Content-Type': 'multipart/formdata',
        },
      })

      return {
        title: 'success',
        message: 'update room',
      }
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },
}

export const mutations = {
  setRooms(state, payload) {
    state.rooms = payload
  },
}
