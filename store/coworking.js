import { request, requestOpen } from '../plugins/axios'

import { generateQueryString } from '../common/queryGenerator'
import { errorNotif, toFormData } from '../common/utils'

export const state = () => ({
  coworking: {},
})

export const getters = {
  getCoworking(state) {
    return state.coworking
  },
}

export const actions = {
  async fetchCoworking(context, payload) {
    try {
      const query = generateQueryString(payload.params)
      const res = await requestOpen({
        url: `articles?${query}`,
        method: 'get',
      })

      const section = res.data.data.data
      context.commit(
        'setCoworking',
        section.find((el) => el.type == 'COWORKING')
      )
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },
  async update(context, payload) {
    try {
      const formData = toFormData(payload)
      await request({
        url: `articles/${payload.id}`,
        method: 'post',
        headers: {
          'Content-Type': 'multipart/formdata',
        },
        data: formData,
      })

      return {
        title: 'success',
        message: 'update data',
      }
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },
}

export const mutations = {
  setCoworking(state, payload) {
    state.coworking = payload
  },
}
