import { request, requestOpen } from '../plugins/axios'

import { generateQueryString } from '../common/queryGenerator'
import { errorNotif, toFormData } from '../common/utils'

export const state = () => ({
  gallery: [],
})

export const getters = {
  getGallery(state) {
    return state.gallery
  },
}

export const actions = {
  async fetchGallery(context, payload) {
    try {
      const query = generateQueryString(payload.params)
      const res = await requestOpen({
        url: `images?${query}`,
        method: 'get',
      })
      context.commit('setGallery', res.data.data)
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },

  async uploadGallery(context, payload) {
    try {
      const data = toFormData(payload)
      await request({
        url: `image`,
        method: 'post',
        data: data,
        headers: {
          'Content-Type': 'multipart/formdata',
        },
      })

      return {
        title: 'success',
        message: 'upload image',
      }
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },

  async deleteGallery(context, payload) {
    try {
      await request({
        url: `image/${payload.id}`,
        method: 'delete',
      })

      return {
        title: 'success',
        message: 'delete image',
      }
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },
}

export const mutations = {
  setGallery(state, payload) {
    if (payload.current_page < 2) {
      state.gallery = { ...payload }
    } else {
      const oldG = state.gallery
      const newG = payload

      newG.data = oldG.data.concat(payload.data)
      state.gallery = newG
    }
  },
}
