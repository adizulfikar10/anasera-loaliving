import { errorNotif, toFormData } from '../common/utils'
import { request, requestOpen } from '../plugins/axios'

import { generateQueryString } from '../common/queryGenerator'

export const state = () => ({
  articles: {
    data: [],
  },
  otherArticles: [],
})

export const getters = {
  getArticle(state) {
    return state.articles
  },
  getOtherArticle(state) {
    return state.otherArticles
  },
}

export const actions = {
  async fetchOtherArticle(context, payload) {
    try {
      const query = generateQueryString(payload.params)
      const res = await requestOpen({
        url: `articles/${payload.id}/other?${query}`,
        method: 'get',
      })
      context.commit('setOtherArticle', res.data.data)
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },

  async fetchArticle(context, payload) {
    try {
      const query = generateQueryString(payload.params)
      const res = await requestOpen({
        url: `articles?${query}`,
        method: 'get',
      })
      context.commit('setArticle', res.data.data)
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },

  async storeArticle(context, payload) {
    try {
      const formData = toFormData(payload)
      const res = await request({
        url: `articles`,
        method: 'post',
        headers: {
          'Content-Type': 'multipart/formdata',
        },
        data: formData,
      })

      return {
        title: 'success',
        message: res.data.message,
      }
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },

  async updateArticle(context, payload) {
    try {
      const formData = toFormData(payload)
      const res = await request({
        url: `articles/${payload.id}`,
        method: 'post',
        headers: {
          'Content-Type': 'multipart/formdata',
        },
        data: formData,
      })

      return {
        title: 'success',
        message: res.data.message,
      }
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },

  async deleteArticle(context, payload) {
    try {
      const res = await request({
        url: `articles/${payload.id}`,
        method: 'delete',
      })

      return {
        title: 'success',
        message: res.data.message,
      }
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },
}

export const mutations = {
  setArticle(state, payload) {
    if (payload.current_page < 2) {
      state.articles = { ...payload }
    } else {
      const oldG = state.articles
      const newG = payload

      newG.data = oldG.data.concat(payload.data)
      state.articles = newG
    }
  },

  setOtherArticle(state, payload) {
    state.otherArticles = payload
  },
}
