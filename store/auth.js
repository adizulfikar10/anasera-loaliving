import { request, requestAuth } from '../plugins/axios'

export const state = () => ({
  user: [],
})

export const getters = {
  getUser(state) {
    return state.user
  },
}

export const actions = {
  async login(context, payload) {
    try {
      const data = {
        email: payload.email,
        password: payload.password,
      }
      const res = await requestAuth({
        url: `auth/login`,
        method: 'post',
        data: data,
      })

      context.commit('setUser', res.data.data)
      // set token
      const authUser = { ...res.data.data, login_time: new Date() }
      window.localStorage.setItem('loalivingsoloauth', JSON.stringify(authUser))

      return {
        title: 'success',
        message: 'hay admin 👋🏻',
      }
    } catch (error) {
      if (error.response) {
        return { title: 'error', message: error.response.data.message }
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },

  async updatePassword(context, payload) {
    try {
      await request({
        url: `auth/change-password`,
        method: 'post',
        data: payload,
      })

      return {
        title: 'success',
        message: 'change password, please re-login',
      }
    } catch (error) {
      if (error.response) {
        return { title: 'error', message: error.response.data.message }
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },
}

export const mutations = {
  setUser(state, payload) {
    state.user = payload
  },
}
