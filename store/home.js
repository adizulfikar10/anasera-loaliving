import { errorNotif, getToken, toFormData } from '../common/utils'

import { generateQueryString } from '../common/queryGenerator'
import { request, requestOpen } from '../plugins/axios'

export const state = () => ({
  sliders: [],
  rooms: [],
  reviews: [],
})

export const getters = {
  getSliders(state) {
    return state.sliders
  },
  getRooms(state) {
    return state.rooms
  },
  getReviews(state) {
    return state.reviews
  },
}

export const actions = {
  async fetchSliders(context, payload) {
    try {
      const query = generateQueryString(payload.params)
      const res = await requestOpen({
        url: `images?${query}`,
        method: 'get',
      })
      context.commit('setSliders', res.data.data)
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },

  async deleteSlider(context, payload) {
    try {
      await request({
        url: `image/${payload.id}`,
        method: 'delete',
      })

      return {
        title: 'success',
        message: 'delete slider',
      }
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },

  async fetchRooms(context, payload) {
    try {
      const query = generateQueryString(payload.params)
      const res = await requestOpen({
        url: `articles?${query}`,
        method: 'get',
      })
      context.commit('setRooms', res.data)
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },

  async fetchReviews(context, payload) {
    try {
      const query = generateQueryString(payload.params)
      const res = await requestOpen({
        url: `reviews?${query}`,
        method: 'get',
      })
      context.commit('setReviews', res.data.data)
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },

  async updateReviews(context, payload) {
    try {
      const id = payload.id ? '/' + payload.id : ''
      await request({
        url: `reviews${id}`,
        method: 'post',
        data: payload,
      })

      return {
        title: 'success',
        message: id !== '' ? 'update review' : 'create review',
      }
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },

  async uploadSlider(context, payload) {
    try {
      const data = toFormData(payload)
      await request({
        url: `image`,
        method: 'post',
        data: data,
        headers: {
          'Content-Type': 'multipart/formdata',
        },
      })

      return {
        title: 'success',
        message: 'add slider',
      }
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },

  async deleteReviews(context, payload) {
    try {
      await request({
        url: `reviews/${payload.id}`,
        method: 'delete',
      })

      return {
        title: 'success',
        message: 'delete review',
      }
    } catch (error) {
      if (error.response) {
        return errorNotif(error.response.data.errors)
      } else {
        return { title: 'error', message: error.message }
      }
    }
  },
}

export const mutations = {
  setSliders(state, payload) {
    state.sliders = payload.data
  },

  setRooms(state, payload) {
    state.rooms = payload.data
  },

  setReviews(state, payload) {
    state.reviews = payload.data
  },
}
