import axios from 'axios'
import { getToken } from '~/common/utils'

export const request = axios.create({
  baseURL: process.env.APP_API_URL,
  timeout: 15000,
})

export const requestAuth = axios.create({
  baseURL: process.env.APP_API_URL,
  timeout: 15000,
})

export const requestOpen = axios.create({
  baseURL: process.env.APP_API_URL + '/public',
  timeout: 15000,
})

request.interceptors.request.use(
  (config) => {
    config.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + getToken(),
    }
    return config
  },
  (error) => {
    Promise.reject(error)
  }
)

requestAuth.interceptors.request.use(
  (config) => {
    config.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    }
    return config
  },
  (error) => {
    Promise.reject(error)
  }
)
