export const getToken = () => {
  const auth = JSON.parse(localStorage.getItem('loalivingsoloauth'))
  if (!auth) return undefined

  const hours = auth.expires_in / 60 / 60 // in hours
  const date = new Date(auth.login_time)
  const now = new Date()
  date.setHours(date.getHours() + hours)

  if (now > date) {
    localStorage.removeItem('loalivingsoloauth')
    return undefined
  }

  return auth.access_token
}

export const toFormData = (payload) => {
  const formData = new FormData()
  Object.keys(payload).forEach((key) => {
    if (Array.isArray(payload[key])) {
      payload[key].forEach((el) => {
        formData.append(key + '[]', el)
      })
    } else {
      formData.append(key, payload[key])
    }
  })

  return formData
}

export const errorNotif = (payload) => {
  const errors = payload
  let errorMessage = ''

  Object.keys(errors).map((el) => {
    errorMessage = errors[el].toString()
  })

  return {
    title: 'error',
    message: errorMessage,
  }
}
