export const generateQueryString = (params) => {
  const queries = []
  if (params.page) {
    queries.push(`page=${params.page}`)
  }
  if (params.per_page) {
    queries.push(`per_page=${params.per_page}`)
  }
  if (params.orderBy) {
    queries.push(`orderBy=${params.orderBy}`)
  }
  if (params.sortBy) {
    queries.push(`sortBy=${params.sortBy}`)
  }
  if (params.p) {
    queries.push(`p=${params.p}`)
  }
  if (params.type) {
    queries.push(`type=${params.type}`)
  }
  if (params.category) {
    queries.push(`category=${params.category}`)
  }
  if (params.limit) {
    queries.push(`limit=${params.limit}`)
  }

  // queries.push('cache=0')
  const queryString = queries.join('&')
  return queryString
}
